# Fedex Assignment

# How to run
Make sure docker is installed, then simply run the following command
```bash
./start.sh
```
Then check: http://localhost:8080/swagger-ui.html

# How to stop
Simply run
 ```bash
 ./stop.sh
 ```