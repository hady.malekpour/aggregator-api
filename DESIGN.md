Solution is implemented with spring webflux and projectreactor,


* AbstractBulkApiCall is implemented to handle the logics for queueing requests and publishing responses, all apiClients inherit from it 
* AbstractBulkApiCall has two Sinks.many(), first requestQueue (all requests go here) and second responsePublisher (all responses here) 
* requestQueue will buffer all incoming requests and use bufferTimeout method to submit the calls after five requests arrived or oldest request waited for 5s
* we use responsePublisher#asFlux, then filter only responses that mach with the request and return it as Mono
* AggregatorService, has the responsibility of waiting for all three Mono to complete and merge them using Mono#zip 

