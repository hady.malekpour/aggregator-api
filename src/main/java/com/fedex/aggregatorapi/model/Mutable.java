package com.fedex.aggregatorapi.model;

import lombok.Data;

@Data
public class Mutable<T> {
    private T value;
}
