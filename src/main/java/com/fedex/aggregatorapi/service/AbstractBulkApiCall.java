package com.fedex.aggregatorapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.aggregatorapi.model.AggregatorApiProperties;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractBulkApiCall<T> {
    protected Sinks.Many<String> requestQueue = Sinks.many().unicast().onBackpressureBuffer();
    protected Sinks.Many<Map<String, Optional<T>>> responsePublisher = Sinks.many().multicast().onBackpressureBuffer();
    protected WebClient webClient;
    protected final AggregatorApiProperties aggregatorApiProperties;
    protected final ObjectMapper objectMapper;

    public Mono<Map<String, Optional<T>>> callApi(List<String> input) {
        if (CollectionUtils.isEmpty(input)) {
            return Mono.just(new HashMap<>());
        }
        var request = input.stream().distinct().collect(Collectors.toList());
        return readFromResponsePublisher(request).doFirst(() -> {
            request.stream().forEach(i -> requestQueue.tryEmitNext(i));
        });
    }

    private Mono<Map<String, Optional<T>>> readFromResponsePublisher(List<String> request) {
        return Mono.from(responsePublisher
                .asFlux()
                .log()
                .flatMap(item -> Flux.fromStream(item.entrySet().stream()))
                .filter(item -> request.contains(item.getKey()))
                .take(request.size())
                .collectMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @PostConstruct
    public void init() {
        webClient = WebClient.create(getBaseUrl());
        requestQueue.asFlux().bufferTimeout(5, Duration.ofSeconds(5)).log().subscribe(this::callExternalApi);
        responsePublisher.asFlux().subscribe();
    }

    private void callExternalApi(List<String> input) {
        var request = input.stream().distinct().collect(Collectors.toList());
        webClient.get()
                .uri(getUri(), request.stream().sorted().collect(Collectors.joining(",")))
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<Map<String, Optional<T>>>() {
                })
                .timeout(aggregatorApiProperties.getTimeout())
                .log()
                .onErrorReturn(request.stream().collect(Collectors.toMap(Function.identity(), v -> Optional.empty())))
                .subscribe(v -> {
                    responsePublisher.tryEmitNext(v);
                });
    }

    abstract String getBaseUrl();

    abstract String getUri();
}
