package com.fedex.aggregatorapi.service;

import com.fedex.aggregatorapi.model.AggregatedResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class AggregatorService {

    private final ShipmentsApiClient shipmentsApiClient;
    private final TrackApiClient trackApiClient;
    private final PricingApiClient pricingApiClient;

    public Mono<AggregatedResult> callAndAggregate(List<String> pricing, List<String> track, List<String> shipments) {
        Mono output = Mono.zip(pricingApiClient.callApi(pricing),
                        trackApiClient.callApi(track),
                        shipmentsApiClient.callApi(shipments))
                .map(tuple -> new AggregatedResult(tuple.getT1(), tuple.getT2(), tuple.getT3()));
        return output;
    }
}
