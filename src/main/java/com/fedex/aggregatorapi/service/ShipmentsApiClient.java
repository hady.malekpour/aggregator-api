package com.fedex.aggregatorapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.aggregatorapi.model.AggregatorApiProperties;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipmentsApiClient extends AbstractBulkApiCall<List<String>> {

    public ShipmentsApiClient(AggregatorApiProperties aggregatorApiProperties, ObjectMapper objectMapper) {
        super(aggregatorApiProperties, objectMapper);
    }

    @Override
    String getBaseUrl() {
        return aggregatorApiProperties.getShipmentsBaseUrl();
    }

    @Override
    String getUri() {
        return aggregatorApiProperties.getShipmentsUri();
    }
}
