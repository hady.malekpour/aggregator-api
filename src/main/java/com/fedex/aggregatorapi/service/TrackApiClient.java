package com.fedex.aggregatorapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.aggregatorapi.model.AggregatorApiProperties;
import org.springframework.stereotype.Service;

@Service
public class TrackApiClient extends AbstractBulkApiCall<String> {

    public TrackApiClient(AggregatorApiProperties aggregatorApiProperties, ObjectMapper objectMapper) {
        super(aggregatorApiProperties, objectMapper);
    }

    @Override
    String getBaseUrl() {
        return aggregatorApiProperties.getTrackBaseUrl();
    }

    @Override
    String getUri() {
        return aggregatorApiProperties.getTrackUri();
    }
}
