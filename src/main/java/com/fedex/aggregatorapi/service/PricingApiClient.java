package com.fedex.aggregatorapi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.aggregatorapi.model.AggregatorApiProperties;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PricingApiClient extends AbstractBulkApiCall<BigDecimal> {

    public PricingApiClient(AggregatorApiProperties aggregatorApiProperties, ObjectMapper objectMapper) {
        super(aggregatorApiProperties, objectMapper);
    }

    @Override
    String getBaseUrl() {
        return aggregatorApiProperties.getPricingBaseUrl();
    }

    @Override
    String getUri() {
        return aggregatorApiProperties.getPricingUri();
    }
}
