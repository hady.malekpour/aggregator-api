package com.fedex.aggregatorapi.controller;

import com.fedex.aggregatorapi.model.AggregatedResult;
import com.fedex.aggregatorapi.service.AggregatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/aggregation")
public class AggregationController {

    @Autowired
    private AggregatorService aggregatorService;

    @GetMapping
    Mono<AggregatedResult> get(@RequestParam(required = false) List<String> pricing, @RequestParam(required = false) List<String> track,
                               @RequestParam(required = false) List<String> shipments) {
        return aggregatorService.callAndAggregate(pricing, track, shipments);
    }
}
