package com.fedex.aggregatorapi.service;

import com.fedex.aggregatorapi.model.AggregatedResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.Arrays;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureWireMock(port = 9090)
class AggregatorServiceTest {
    private Duration timeout = Duration.ofSeconds(5);

    @Autowired
    private AggregatorService aggregatorService;

    @Test
    public void testExceed5Secs() {
        stubTwoItems();

        Mono<AggregatedResult> mono = aggregatorService
                .callAndAggregate(Arrays.asList("NL", "CN"), Arrays.asList("1", "2"), Arrays.asList("1", "2"));

        Duration duration = StepVerifier
                .create(mono)
                .thenConsumeWhile(result -> {
                    assertThat(result).isNotNull();
                    assertThat(result.getShipments()).hasSize(2);
                    assertThat(result.getPricing()).hasSize(2);
                    assertThat(result.getTrack()).hasSize(2);
                    return true;
                })
                .verifyComplete();
        assertThat(duration).isGreaterThan(Duration.ofSeconds(5));
    }

    @Test
    public void testIf503Unavailable() {
        stubWith503Unavailable();

        Mono<AggregatedResult> mono = aggregatorService
                .callAndAggregate(Arrays.asList("DE", "BE"), Arrays.asList("3", "4"), Arrays.asList("3", "4"));

        StepVerifier
                .create(mono)
                .thenConsumeWhile(result -> {
                    assertThat(result).isNotNull();
                    assertThat(result.getShipments()).hasSize(2);
                    assertThat(result.getPricing()).hasSize(2);
                    assertThat(result.getTrack()).hasSize(2);
                    return true;
                })
                .thenCancel()
                .verifyLater()
                .verify();
    }


    @Test
    public void testRunningConcurrent() throws InterruptedException {
        Integer delayMilliseconds = 1000;
        stubFiveAndTwoItemsAndReplyWithDelay(delayMilliseconds);

        Mono<AggregatedResult> mono1 = aggregatorService
                .callAndAggregate(Arrays.asList("NL", "CN"), Arrays.asList("1", "2", "3", "4", "5"), null);
        Mono<AggregatedResult> mono2 = aggregatorService
                .callAndAggregate(Arrays.asList("US", "CA", "UK"), null, Arrays.asList("1", "2", "3", "4", "5"));

        //We are waiting to make sure the first five does not get mixed with next items, this is because we mock the external service and expecting these fives come together
        Thread.currentThread().sleep(50);

        Mono<AggregatedResult> mono3 = aggregatorService
                .callAndAggregate(Arrays.asList("KR", "JP"), Arrays.asList("6", "7"), Arrays.asList("6", "7"));

        StepVerifier verifier1 = StepVerifier
                .create(mono1)
                .thenConsumeWhile(result -> {
                    assertThat(result).isNotNull();
                    assertThat(result.getPricing()).hasSize(2);
                    assertThat(result.getTrack()).hasSize(5);
                    assertThat(result.getShipments()).hasSize(0);
                    return true;
                }).thenCancel()
                .verifyLater();

        StepVerifier verifier2 = StepVerifier
                .create(mono2)
                .expectSubscription()
                .thenConsumeWhile(result -> {
                    assertThat(result).isNotNull();
                    assertThat(result.getPricing()).hasSize(3);
                    assertThat(result.getTrack()).hasSize(0);
                    assertThat(result.getShipments()).hasSize(5);
                    return true;
                }).thenCancel()
                .verifyLater();

        Duration duration1 = verifier1.verify();
        Duration duration2 = verifier2.verify();

        //To show we call external services in one batch instead of two different single calls
        assertThat(duration1).isLessThan(Duration.ofSeconds(2 * delayMilliseconds));
        assertThat(duration2).isLessThan(Duration.ofSeconds(2 * delayMilliseconds));


        StepVerifier verifier3 = StepVerifier
                .create(mono3)
                .expectSubscription()
                .thenConsumeWhile(result -> {
                    assertThat(result).isNotNull();
                    assertThat(result.getPricing()).hasSize(2);
                    assertThat(result.getTrack()).hasSize(2);
                    assertThat(result.getShipments()).hasSize(2);
                    return true;
                }).thenCancel()
                .verifyLater();
        Duration duration3 = verifier3.verify();

        //To show it wait 5 seconds before making a call
        assertThat(duration3).isGreaterThan(Duration.ofMillis(Duration.ofSeconds(5).toMillis() + delayMilliseconds));
    }

    @Test
    public void testMoreThanBatchSize() throws InterruptedException {
        Integer delayMilliseconds = 1000;
        stubFiveAndTwoItemsAndReplyWithDelay(delayMilliseconds);

        Mono<AggregatedResult> mono1 = aggregatorService
                .callAndAggregate(Arrays.asList("NL", "CN", "US", "CA", "UK", "KR", "JP"), Arrays.asList("1", "2", "3", "4", "5", "6", "7"), Arrays.asList("1", "2", "3", "4", "5", "6", "7"));

        StepVerifier verifier1 = StepVerifier
                .create(mono1)
                .thenConsumeWhile(result -> {
                    assertThat(result).isNotNull();
                    assertThat(result.getPricing()).hasSize(7);
                    assertThat(result.getTrack()).hasSize(7);
                    assertThat(result.getShipments()).hasSize(7);
                    return true;
                }).thenCancel()
                .verifyLater();
        Duration duration1 = verifier1.verify();

        //To show we call external services in one batch instead of two different single calls
        assertThat(duration1).isGreaterThan(Duration.ofMillis(delayMilliseconds + timeout.toMillis()));
        assertThat(duration1).isLessThan(Duration.ofMillis(delayMilliseconds + timeout.toMillis() + 1000L));
    }

    private void stubTwoItems() {
        createStub("/pricing?q=CN%2CNL", "{\"CN\": 2, \"NL\": 1}");
        createStub("/track?q=1%2C2", "{\"1\": \"NEW\", \"2\": \"COLLECTING\"}");
        createStub("/shipments?q=1%2C2", "{\"1\": [\"box\", \"box\", \"pallet\"], \"2\": [\"envelope\"]}");
    }

    private void stubFiveAndTwoItemsAndReplyWithDelay(Integer delayMilliseconds) {
        createStub("/pricing?q=CA%2CCN%2CNL%2CUK%2CUS", "{\"CA\": 1, \"CN\": 2, \"NL\": 1, \"UK\": 2, \"US\": 1}", delayMilliseconds);
        createStub("/track?q=1%2C2%2C3%2C4%2C5", "{\"1\": \"NEW\", \"2\": \"COLLECTING\", " +
                "\"3\": \"NEW\", \"4\": \"COLLECTING\", " +
                "\"5\": \"DELIVERING\"}", delayMilliseconds);
        createStub("/shipments?q=1%2C2%2C3%2C4%2C5", "{\"1\": [\"box\", \"box\", \"pallet\"], " +
                "\"2\": [\"envelope\"], \"3\": [\"envelope\"], \"4\": [\"box\"], " +
                "\"5\": [\"envelope\"]}", delayMilliseconds);

        createStub("/pricing?q=JP%2CKR", "{\"KR\": 1, \"JP\": 2}", delayMilliseconds);
        createStub("/track?q=6%2C7", "{\"6\": \"NEW\", \"7\": \"COLLECTING\"}", delayMilliseconds);
        createStub("/shipments?q=6%2C7", "{\"6\": [\"box\"], " + "\"7\": [\"envelope\"]}", delayMilliseconds);
    }

    private void createStub(String path, String response) {
        createStub(path, response, 0);
    }

    private void createStub(String path, String response, Integer delay) {
        stubFor(get(urlEqualTo(path))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withFixedDelay(delay)
                        .withBody(response)));
    }

    private void stubWith503Unavailable() {
        stubFor(get(urlEqualTo("/pricing?q=BE%2CDE")).willReturn(aResponse().withStatus(503)));
        stubFor(get(urlEqualTo("/track?q=3%2C4")).willReturn(aResponse().withStatus(503)));
        stubFor(get(urlEqualTo("/shipments?q=3%2C4")).willReturn(aResponse().withStatus(503)));
    }

}